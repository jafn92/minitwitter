json.extract! post, :id, :post, :user_id, :comment_id, :like_id, :created_at, :updated_at
json.url post_url(post, format: :json)
