class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :post
      t.integer :user_id
      t.integer :comment_id
      t.integer :like_id

      t.timestamps
    end
  end
end
